#!/usr/bin/env bash

if [ -z $(which wget) ]; then
  echo 'Please install wget before attempting to run this script.'
fi

local_file_location=~/Miniconda3-latest-Linux-x86_64.sh
remote_file_location="https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh"
install_location=~/.local/share/miniconda3

wget -O ${local_file_location} "${remote_file_location}"
bash ${local_file_location} -b -p ${install_location}
cat >> ~/.bashrc << EOF

# Adding minicond3 path
export PATH="${install_location}/bin:\$PATH"

EOF
rm ${local_file_location}

