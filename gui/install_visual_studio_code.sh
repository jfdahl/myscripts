#!/usr/bin/env bash
if [ "$(id -u)" == "0" ]; then
    install_location=/usr/local/share
    exec_location=/usr/bin
else
    install_location=~/.local/share
    exec_location=~/bin
fi
[ -d ${exec_location} ] || mkdir -p ${exec_location}
[ -d ${install_location}/applications ] || mkdir -p ${install_location}/applications

local_file_location=/tmp/vscode.tar.gz

linux64='620884'
linux32='620885'
remote_file_location="https://go.microsoft.com/fwlink/?LinkID=${linux64}"

sudo apt install libgconf-2-4

wget -O ${local_file_location} "${remote_file_location}"
tar -xzf ${local_file_location} -C ${install_location}
ln -s ${install_location}/VSCode-linux-x64/bin/code ${exec_location}/code
rm ${local_file_location}

# Setup desktop file and menu entries
cat >> ${install_location}/applications/code.desktop << EOF
[Desktop Entry]
Name=Visual Studio Code
Comment=Text Editor.
GenericName=Text Editor
Exec=${install_location}/VSCode-linux-x64/bin/code %F
Icon=${install_location}/VSCode-linux-x64/resources/app/resources/linux/code.png
Type=Application
StartupNotify=true
Categories=Development;Accessories
Terminal=false
MimeType=text/plain
EOF
