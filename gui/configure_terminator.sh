#!/usr/bin/env bash

if [ -z $(which terminator) ]; then
    echo "Please install terminator before attempting to configure it."
    exit 1
fi

config_file_location=${HOME}/.config/terminator
mkdir -p ${config_file_location}

cat >> ${config_file_location}/config << EOF
[global_config]
[keybindings]
[profiles]
  [[default]]
    login_shell = True
    use_system_font = False
    font = DejaVu Sans Mono 12
    show_titlebar = False
[layouts]
  [[default]]
    [[[child1]]]
      type = Terminal
      parent = window0
    [[[window0]]]
      type = Window
      parent = ""
[plugins]
EOF
