#!/usr/bin/env bash
if [ "$(id -u)" == "0" ]; then
    echo "Run this as the user so that the username can be captured."
    echo "The user password will be requested for executing the command with sudo."
    exit 1
fi

if [[ $(lspci | grep VirtualBox) ]]; then
  VBOX=true
else
  echo "VirtualBox was not detected."
  exit 1
fi


sudo apt install -y dkms build-essential
 # TODO: If Debian, also install linux-headers-amd64


script_location=$(find /media/${user} -name VBoxLinuxAdditions.run)
[[ -f $script_location ]] || \
sudo mount -o loop /dev/sr0 /mnt && \
script_location=$(find /mnt -name VBoxLinuxAdditions.run) || \
exit 1

sudo bash << EOF
${script_location} && \
usermod -a -G vboxsf ${user}
eject /dev/sr0
reboot
EOF
