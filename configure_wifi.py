#!/usr/bin/env python3

import os


def select_wireless_network():
    ssids_object = os.popen("nmcli -f SSID dev wifi")
    ssids_list = []
    print('Detected wireless networks:')
    for i, v in enumerate(ssids_object):
        v = v.strip().strip("'")
        ssids_list.append(v)
        if i == 0:
            continue
        print('%3s: %s' % (i, v))

    network_id = int(input("Enter network number: "))
    network_name = ssids_list[network_id]
    return network_name


def connect_to_selected_wireless_network(network_name):
    command = f'nmcli -a dev wifi connect {network_name}'
    network_pwd = preferred_networks.get(network_name, None)

    if network_pwd:
        command += f' password {network_pwd}'
    os.popen(command)


preferred_networks = {
    'jnkguest': 'VGhpczFzZ3Vlc3QK',
    }
network_name = select_wireless_network()
connect_to_selected_wireless_network(network_name)

