#!/usr/bin/env bash
if [ "$(id -u)" != "0" ]; then
    echo "Please execute this as root or with sudo."
    exit 1
fi


# Check for WIFI adapter
pci_wifi_count=$(lspci | egrep -ic 'wifi|wlan|wireless')
usb_wifi_count=$(lsusb | egrep -ic 'wifi|wlan|wireless')
wifi_count=$(( $pci_wifi_count + $usb_wifi_count ))
[ ${wifi_count} -gt 0 ] && WIFI=true || WIFI=false

if [ ! $WIFI ]; then exit 1; fi

# Disable IPv6
if [ -z /etc/sysctl.d/20-disable-ipv6.conf ]; then
cat > /etc/sysctl.d/20-disable-ipv6.conf << EOF
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
EOF
fi

apt install -y dialog network-manager rfkill wireless-tools wpasupplicant
systemctl start NetworkManager
systemctl enable NetworkManager

cat >> /home/public/bin/env.sh << EOF
alias scanap='nmcli d wifi list'
EOF
