#!/usr/bin/env bash
if [ "$(id -u)" != "0" ]; then
    echo "Please execute this as root or with sudo."
    exit 1
fi

if [ -z $(which git) ]; then
    echo "Please install git before installing atom."
    exit 1
fi

if [ -f $(which atom) ]; then
    update=true
else
    update=false
fi

local_file_location=/tmp/atom.deb
remote_file_location=https://atom.io/download/deb

wget -O ${local_file_location} ${remote_file_location} && \
dpkg -i ${local_file_location} && \
rm ${local_file_location}
apt-get install -yf

$update || apm install minimap highlight-selected cobalt2-syntax
