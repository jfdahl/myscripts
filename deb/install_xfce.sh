#!/bin/bash
if [ "$(id -u)" != "0" ]; then
    echo "Please execute this as root or with sudo."
    exit 1
fi

packages="xfce4 xfce4-goodies gvfs alsa-utils xscreensaver mousepad dconf-cli"

apt install -y ${packages}
