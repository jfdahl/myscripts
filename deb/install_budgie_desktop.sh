#!/bin/bash
if [ "$(id -u)" != "0" ]; then
    echo "Please execute this as root or with sudo."
    exit 1
fi

packages="xorg xorg-server gvfs alsa-utils lxdm budgie-desktop"

apt install -y ${packages}
